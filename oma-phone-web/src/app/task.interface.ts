export interface Task {
  category: string;
  id: string;
  phone: string;
  plz: string;
  sid: string;
  timestamp: string;
}
