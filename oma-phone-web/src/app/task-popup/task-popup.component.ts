import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Task } from "../task.interface";

@Component({
  selector: "app-task-popup",
  templateUrl: "./task-popup.component.html",
  styleUrls: ["./task-popup.component.css"]
})
export class TaskPopupComponent implements OnInit {
  @Input() task: Task;

  @Output() closeEvent: EventEmitter<void> = new EventEmitter();

  constructor() {}

  ngOnInit() {}
}
