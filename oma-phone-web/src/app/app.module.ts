import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { TaskComponent } from "./task/task.component";
import { TaskListComponent } from "./task-list/task-list.component";
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./login/login.component";
import { SignUpComponent } from "./sign-up/sign-up.component";
import { HttpClientModule } from "@angular/common/http";
import { CategoryParserPipe } from "./category.pipe";
import { FaqComponent } from "./faq/faq.component";
import { OnboardingComponent } from "./onboarding/onboarding.component";
import { hideNumberPipe } from './hideNumber.pipe';
import { TaskPopupComponent } from './task-popup/task-popup.component';
import { ImpressumBannerComponent } from './impressum-banner/impressum-banner.component';
import { ImpressumComponent } from './impressum/impressum.component';

@NgModule({
  declarations: [
    AppComponent,
    TaskComponent,
    TaskListComponent,
    HomeComponent,
    LoginComponent,
    SignUpComponent,
    CategoryParserPipe,
    FaqComponent,
    OnboardingComponent,
    hideNumberPipe,
    TaskPopupComponent,
    ImpressumComponent,
    ImpressumBannerComponent
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
