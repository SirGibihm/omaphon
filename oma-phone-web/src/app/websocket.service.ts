import { Injectable } from "@angular/core";
import { Subject, Observable, Observer } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class WebsocketService {
  private WSS_URL = "ws://omaphon.de:8081";

  public subject$: Subject<any>;

  constructor() {
    const ws = new WebSocket(this.WSS_URL);

    const observable = Observable.create((obs: Observer<MessageEvent>) => {
      ws.onmessage = obs.next.bind(obs);
      ws.onerror = obs.error.bind(obs);
      ws.onclose = obs.complete.bind(obs);
      return ws.close.bind(ws);
    });
    const observer = {
      next: (data: Object) => {
        if (ws.readyState === WebSocket.OPEN) {
          ws.send(JSON.stringify(data));
        }
      }
    };
    this.subject$ = Subject.create(observer, observable);
  }
}
