import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Task } from "../task.interface";

@Component({
  selector: "app-task",
  templateUrl: "./task.component.html",
  styleUrls: ["./task.component.css"]
})
export class TaskComponent implements OnInit {
  @Input() task: Task;

  @Output() accept: EventEmitter<Task> = new EventEmitter();
  @Output() reject: EventEmitter<Task> = new EventEmitter();

  constructor() {}

  ngOnInit() {}
}
