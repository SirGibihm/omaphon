import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TaskListComponent } from "./task-list/task-list.component";
import { HomeComponent } from "./home/home.component";
import { SignUpComponent } from "./sign-up/sign-up.component";
import { FaqComponent } from "./faq/faq.component";
import { OnboardingComponent } from "./onboarding/onboarding.component";
import { ImpressumComponent } from "./impressum/impressum.component";

const routes: Routes = [
  { path: "tasks", component: TaskListComponent },
  { path: "home", component: HomeComponent },
  { path: "signUp", component: SignUpComponent },
  { path: "faq", component: FaqComponent },
  { path: "onboarding", component: OnboardingComponent },
  { path: "impressum", component: ImpressumComponent },
  //{ path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: "**", component: HomeComponent }
];

@NgModule({
  //imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: "top" })], // fragment routing doesn't work...
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

// const routes: Routes = [
//   { path: 'heroes', component: HeroesComponent }
// ];
