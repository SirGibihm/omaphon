import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImpressumBannerComponent } from './impressum-banner.component';

describe('ImpressumBannerComponent', () => {
  let component: ImpressumBannerComponent;
  let fixture: ComponentFixture<ImpressumBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImpressumBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImpressumBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
