import { Pipe, PipeTransform } from '@angular/core';


@Pipe({name: 'hideNumber'})
export class hideNumberPipe implements PipeTransform {
  transform(numberString: string): string {
    return numberString.substr(0,6)+"******";;
  }
}
