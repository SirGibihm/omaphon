import { Pipe, PipeTransform } from '@angular/core';

enum Categorys {
  'Lebensmittel / Hygieneartikel einkaufen' = 1,
  'Arzneimittel besorgen' = 2,
  'Gang zur Post' = 3,
  'Betreuung von Tieren' = 4,
  'Grabpflege' = 5,
}

@Pipe({name: 'categoryParser'})
export class CategoryParserPipe implements PipeTransform {
  transform(category: string): string {
    const c = parseInt(category);
    return Categorys[c];
  }
}
