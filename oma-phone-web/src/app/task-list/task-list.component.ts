import { Component, OnInit } from "@angular/core";
import { Task } from "../task.interface";
import { WebsocketService } from "../websocket.service";
import { map, tap, flatMap, mergeMap } from "rxjs/operators";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-task-list",
  templateUrl: "./task-list.component.html",
  styleUrls: ["./task-list.component.css"]
})
export class TaskListComponent implements OnInit {
  public tasks$: Observable<Task[]>;
  public selectedTask: Task;

  constructor(
    private httpClient: HttpClient,
    private wsService: WebsocketService
  ) {}

  ngOnInit() {
    this.tasks$ = this.wsService.subject$.pipe(
      map(x => JSON.parse(x.data)), // Parse timestamps as Javscript Timestamp
      map(x => x.reverse())
    );
  }

  public accept(task: Task) {
    console.log("accept");
    this.selectedTask = task;
    // this.httpClient.post(`/accept?taskId=${task.id}`);
  }

  public reject(event) {
    console.log("reject", event);
  }

  public close() {
    this.selectedTask = null;
  }
}
