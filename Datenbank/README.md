# Datenbanken und Datenstrukturen
Eine zentrale Datenbnank, basierend auf postgreSQL läuft auf dem Server und 
hinterlegt alle Hilfegesuche, welche telefonisch aufgenommen werden. Details 
wie die Daten in der Datenbank gespeichert werden sind dem Ordner "Backend" zu 
entnehmen. 
## Installation und Vorbereitung von postgreSQL
Da es sich bei unserem Server um einen Ubuntu 18.04 LTS handelt lässt sich 
postgreSQL einfach über ```sudo apt install postgresql``` installieren. Dabei
wird automatisch der Nutzer *postgres* angelegt über welchen die Datenbank 
administriert wird. Auch wird ein Service angelegt, welche die Datenbank beim 
Systemstart mit startet. Der Status der Datenbank lässt sich über diesen Service
abfragen.
```bash
sudo service postgresql status
```   
Zugriffe auf die Datenbank sollen über einen Datenbnaknutzer geschehen. Hierfür
wird zunächst auf den Nutzer *posgres* gewechselt. In diesem Nutzerkontext wird 
die Postgres Konsole gestartet und der Datenbankbenutzer *omadbuser* angelegt.
Auch wird eine Datenbank namens *omaphondb* erstellt. Abschließend bekommt der 
Postgres-Nutzer alle Rechte für diese Datenbank. 
```bash
sudo -su postgres
psql
CREATE USER omaphondbuser WITH ENCRYPTED PASSWORD '<passwort>';
CREATE DATABASE omaphondb;
GRANT ALL PRIVILEGES ON DATABASE omaphondb TO omaphondbuser;
```  

## Datenstrukturen
Es sind grundlegend zwei Datenstrukturen in der Datenbank vorgesehen; 
Registrierungsdaten der Helfenden und die Hilfegesuche der Senioren, genannt 
*tasks*. Aufgrund des zeitlichen Rahmens wurde noch kein User Management 
aufgesetzt; die Tabelle der Helfer existiert daher noch nicht.

**tasks**
* Postgres ID (ID SERIAL PRIMARY KEY)
* ID des Telefonanbierters (sid): String mit max. Länge 100
* Zeitstempel des Anrufs (timestamp): String mit max. Länge 100
* Postleitzahl des Anrufers (plz): String mit max. Länge 10
* Telefonnumemr des Anrufers (phone): String mit max. Länge 30
* Hilfsbedarf des Anrufers (category): String mit max. Länge 30  

Die Kategorie des Telefonanbieters wird über eine Zahl-Kodierung übertragen: 
1.  Lebensmittel / Hygieneartikel einkaufen
2.  Arzneimittel besorgen
3.  Gang zur Post
4.  Betreuung von Tieren
5.  Grabpflege

In Postgres wurde die Tabelle mit folgendem Kommando erstellt: 
```bash 
sudo -su postgres
psql -U omaphondbuser -h 127.0.0.1 omaphondb
CREATE TABLE tasks (ID SERIAL PRIMARY KEY, sid VARCHAR(100), timestamp VARCHAR(100), plz VARCHAR(10), phone VARCHAR(30), category VARCHAR(30));
```
Ein manuelles befüllen dieser Tabelle ist ebenfalls über psql shell mit dem 
User *omaphondbuser* möglich:  
```bash
INSERT INTO tasks (sid, timestamp, plz, phone, category) VALUES ('totally unique sid 16', '2020-03-20T10:30:08Z', '47373', '+266636487', '4');
```
