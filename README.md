# omaPhon
PoC für wirvsvirus, Du brauchst Hilfe im Alltag? Ruf einfach an!
## Prototyp von OmaPhon
Derzeit ist ein Prototyp unter [OmaPhon.de](http://OmaPhon.de) erreichbar. 
Die Anmeldung und der Login sind derzeit noch nicht funktional. Es ist möglich
sich mit beliebigen Zugangsdaten einzuloggen. Es folgt eine Weiterleitung zu
allen verfügbaren Tasks und die Möglichkeit sich einen der Tasks zur Bearbeitung
auszuwählen.  
Eine hilfesuchende Person kann über die Rufnummer +1 206 222 2060 Hilfe 
anfordern. Sie wird aufgefordert ihre Postleitzahl und ihr Anliegen über die 
Tasten auf dem Telefon einzugeben. Der Helfer ruft dann nach Auswahl des Tasks
die hilfesuchende Person zurück.
## Informationen über dieses Git
Teil dieses Gitlab Projekts ist ein Issue Board, auf welchem alle Tasks des
Projekts aufgeführt sind. Es findet sich in der linken Spalte der Website unter 
Issues > Board. Wenn ihr einen neuen Task aufschreiben wollt oder etwas 
übernehmen wollt ändert ihr schlicht den Assignee auf euch selbst und das label
von ToDo auf Doing.  
Im Rahmen des Hackathons können nicht alle gewünschten Funktionen implementiert 
werden. Features, welche in der Zukunft erst angegangen werden finden sich unter 
dem Meilenstein "Future Work" und sind mit "Future Work" zu labeln.  
Im Rahmen des Hackathons sind Passwortdaten im git gelandet. Die 
Passwörter wurden geändert.
# Überblick über das Projekt
## Frontend über Angular und nodejs
Über Angular und Nodejs werden die für den Endnutzer notwendigen JS, HTML, und 
CSS Dateien erstellt. Diese werden dann durch einen NGINX Server ausgeliefert. 
Details über das Frontend finden sich [hier](https://gitlab.com/SirGibihm/omaphon/-/tree/master/oma-phone-web).
## Backend 
Das Backend wird über Nodejs realisiert. Die Skripte und wie sie zu verwenden
sind werden [hier](https://gitlab.com/SirGibihm/omaphon/-/tree/master/oma-phone-web) erläutert.
## Datenbank
Es gibt keine eigentlichen Skripte für die Datenbank. Diese muss auf dem
hostenden Server eingerichtet werden. Eine Anleitung findet sich im entsprechenden
[Verzeichnis](https://gitlab.com/SirGibihm/omaphon/-/tree/master/Datenbank).
## Twilio Studio Flow
Es ist im [Twilio Studio](https://www.twilio.com) ein Workflow erstellt worden, der die Omaphon-Hotline 
abbildet. Diese Hotline wird prototypisch von Twilio gehostet & nimmt die 
Eingaben der Hilfesuchenden an. Perspektivisch kann dies in die eigene
Anwendung eingebaut werden. Als Endresultat wird ein simples JSON generiert,
welches alle relevanten Information für unseren Omaphon Prototypen bereithält.
Es folgt folgender Struktur:

```json
{ 
    "sid": "FN8a619b95a27c903d1466b10194ea673f", 
    "timestamp": "2020-03-21T08:30:08Z", 
    "plz": "50968", 
    "phone": "+266696687", 
    "category": 5 
}
```

##API für Hilfsorganisationen/Vereine
Bisher beinhaltet unsere API Schnittstelle folgende Endpunkte:

GET http://omaphon.de:8900/tasks

Dieser Endpunkt gibt eine Liste aller zur Zeit gespeicherten Hilfsbedürftigen
in unserer Datenbank.

Return value:
```json
{ 
    "sid": "FN8a619b95a27c903d1466b10194ea673f", 
    "timestamp": "2020-03-21T08:30:08Z", 
    "plz": "50968", 
    "phone": "+266696687", 
    "category": 5 
}
```

**SID**: Eindeutige ID des Eintrages  
**Timestamp**: Uhrzeit, zu der der Hilfebedarf aufgegeben worden ist  
**PLZ**: Postleitzahl in der sich der Wohnsitz des Hilfsbedürftigen befindet  
**Phone**: Anonymisierte Telefonnummer des Hilfsbedürftigen  
**Category**: Kategorie zu der der Hilfebedarf gehört  
    1: Einkauf von Lebensmitteln & Hygieneartikeln  
    2: Einkauf von Arzneimitteln  
    3: Gang zur Post  
    4: Pflege von Tieren  
    5: Hilfe bei der Grabpflege  