import * as WebSocket from "ws";

export class WebSocketServer {
  wss: WebSocket.Server;
  callback: any;

  constructor(onNewClient: () => void) {
    // initialize the WebSocket server instance
    this.wss = new WebSocket.Server({ port: 8081 });

    this.wss.on("connection", (ws: WebSocket) => {
      //connection is up, let's add a simple simple event
      ws.on("message", (message: string) => {
        //log the received message and send it back to the client
        console.log("received: %s", message);
        //ws.send(`Hello, you sent -> ${message}`);
      });
      onNewClient(); //
    });
  }

  public notifyNewTasks(tasks: string) {
    this.wss.clients.forEach(client => {
      client.send(tasks);
    });
  }
}
