import { TaskCrawler } from "./TaskCrawler";
import { WebSocketServer } from "./websocket";
import { TaskWriter } from "./TaskWriter";
import express = require("express");

const app = express();
app.use(express.json());
const port = 8900;

const crawler = new TaskCrawler();
const writer = new TaskWriter();

const wss = new WebSocketServer(() => {
  notifyClients();
});

const notifyClients = () => {
  // notify all connected clients to reload
  crawler
    .getCurrentTasks()
    .then((tasks: any) => wss.notifyNewTasks(JSON.stringify(tasks)));
};

app.get("/tasks", async (req, res) => {
  console.log(req);
  res.status(200).json(await crawler.getCurrentTasks());
});

// Wenn ein Post Request auf /postTask bei dem o.g. port landet, starte this.pushNewTasks
app.post("/postTask", async (req, res) => {
  const body = req.body;
  //console.log(req);
  console.log(body);
  // Der eigentliche Inhalt des Postrequest von der Telefonieanlage steht entweder in req oder in res, nicht ganz sicher
  await writer
    .pushNewTasks(body)
    .then(_ => {
      res.send("Tasks saved");
    })
    .catch((e: Error) => {
      console.error(e);
      res.status(500); // TODO! remove -> May contain sensitive information -> Security concerns
      res.send(e); // TODO! remove -> May contain sensitive information -> Security concerns
      // res.sendStatus(500);
    });

  notifyClients();
});

// Wenn dieses Object erstellt ist starte einen listener auf localhost
app.listen(port, () => {
  console.log(`server started at http://localhost:${port}`);
});

// curl --header "Content-Type: application/json" --request POST --data '{ "sid": "FN8a619b95a27c903d1466b10194ea673f", "timestamp": "2020-03-21T08:30:08Z", "plz": "50968", "phone": "+266696687", "category": 5 }' http://localhost:8900/postTask
// curl --header "Content-Type: application/json" --request POST --data '{ "sid": "FN8a619b95a27c903d1466b10194ea673f", "timestamp": "2020-03-21T08:30:08Z", "plz": "50968", "phone": "+266696687", "category": 5 }' http://omaphon.de:8900/postTask
