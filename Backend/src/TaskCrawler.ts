const Pool = require("pg").Pool;

export class TaskCrawler {
  private pool: any;
 
  constructor() {
    this.pool = new Pool({
      user: "omaphondbuser",
      host: "localhost",
      database: "omaphondb",
      password: "Rf6G7dcjnJuC",
      port: 5432
    });
  }

  public async getCurrentTasks() {
    return this.pool.connect().then((client: any) => {
      return client
        .query("SELECT * FROM tasks", (err: any, res: any) => res)
        .then((res: any) => {
			client.release()
          	return res.rows;
        })
    });
  }
}
