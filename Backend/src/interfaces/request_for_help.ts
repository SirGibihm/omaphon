export interface Task{
  plz: number;
  place: string;
  timespan: string;
  helpType: string;
  phoneNumber: string;
}

export class request_for_help {

    constructor(
		public plz: string, 
		public time: string,
		public phone_number: number) {
    }
}
