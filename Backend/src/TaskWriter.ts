import express = require("express");
const Pool = require("pg").Pool;

export class TaskWriter {
  private pool: any;

  constructor() {
    this.pool = new Pool({
      user: "omaphondbuser",
      host: "localhost",
      database: "omaphondb",
      password: "Rf6G7dcjnJuC",
      port: 5432
    });
    /*
    this.pool = new Pool({
      user: "testuser",
      host: "localhost",
      database: "omaphondb",
      password: "testpasswort",
      port: 5432
    });
    */
  }

  // Task der die Datenbankverbindung aufbaut und schreibt
  public async pushNewTasks(task: {
    sid: string;
    timestamp: string;
    plz: string;
    phone: string;
    category: number;
  }) {
    const insertStmt = `INSERT INTO tasks (sid, plz, phone, category, timestamp) VALUES ('${task.sid}', '${task.plz}', '${task.phone}', '${task.category}', '${task.timestamp}')`;
    // Verbinde zur Datenbank
    return this.pool.connect().then((client: any) =>
      // Query, die hartcodierte Werte in die Datenbank schreibt. Hier müsste eigentlich vorher das Parsing aus der req/res (s.o.) geschehen
      client.query(insertStmt).then((res: any) => client.release())
    );
  }
}
