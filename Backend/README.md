# Backend des OmaPhone
In diesem Ordner befindet sich das Backend von [OmaPhon](http://omaphon.de). 
Das Backend erfüllt zwei Kernanforderungen. Es verarbeitet die Telefondaten von
Twilio und bedient das Frontend mit den aktuell verfügbaren Tasks. 
## How to use
Wenn das Programm über ```git clone [...]``` herunterlgeladen wurde müssen
navigiert man zunächst in das Verzeichnis des Backend und installiert die node
dependencies. Dann kann das Backend selbst für Testzwecke gestartet werden. Das
saubere Deployment des Backends ist dem geneigten Leser überlassen.
```bash
git clone [...]
cd omaphon/Backend
npm install
npm run dev
```
## Bearbeiten von Twilio Daten (Twilio -> Backend)
Die Hilfesuchenden melden sich über die Twilio Rufnummer. Daraufhin sendet 
Twilio einen HTTP Post auf einen definierten Port der Datenbank. In unserer 
Konfiguration werden die Twilio Posts an *http://omaphon.de/postTask* auf Port 
8900 gesendet. Die Twilio POST Requests bestehen aus einem simplen JSON Body 
mit folgendem Aufbau:  
```JSON
{ 
    "sid": "FNe45ab6f8a8dff2bfa8b46956da4e4e42", 
    "timestamp": "2020-03-22'T'08:15:04Z", 
    "plz": "53129", 
    "phone": "+4916095988349", 
    "category": 5 
}
```
Dieser wird automatisch geparst und in der PostgreSQL Datenbank abgespeichert. 
Die Logik die für das Aufsetzen des Websockets befindet sich in der Datei [index.ts](https://gitlab.com/SirGibihm/omaphon/-/blob/master/Backend/src/index.ts)
Wenn ein entsprechender POST Request auf dem Socket eingeht wird dieser an die 
Klasse [*TaskWriter*](https://gitlab.com/SirGibihm/omaphon/-/blob/master/Backend/src/TaskCrawler.ts)
weitergeleitet, welche eine Verbindung zur
Datenbank öffnet und dann die Daten aus dem JSON in der Datenbank hinterlegt.
Für das Bereitstellung des Websockets wird [*express*](https://expressjs.com)
genutzt; die Verbindung zur PostgrSQL Datenbank wird über [node-postgres](https://node-postgres.com)
umgesetzt. 
## Frontend Anfragen an das Backend
Auch muss das Frontend in der Lage sein die verfügbaren Tasks aus dem Backend
abrufen zu können. Dies geschieht über einen kleinen Websocket, welcher alle 
verbundenen Clients darüber informiert, wenn neue Tasks zur Verfüng stehen. Der 
Websocket ist ebenfalls in der [index.ts](https://gitlab.com/SirGibihm/omaphon/-/blob/master/Backend/src/index.ts)
definiert und greift auf die Klasse [*TaskCrawler*](https://gitlab.com/SirGibihm/omaphon/-/blob/master/Backend/src/TaskCrawler.ts)
zu. Diese Klasse nutzt ebenfalls [node-postgres](https://node-postgres.com)
um alle verfügbaren Tasks aus der Datenbank auszulesen.